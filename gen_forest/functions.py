import random
"""
    Fonction gen_forest
    %       Cette fonction initialise un tableau bi-dimmensionnel
            représentant la forêt avec le nombre de ligne et de
            colonnes passés en arguments ainsi que le pourcentage
            de boisement.
    %IN     row : entier représentant le nombre de lignes souhaitées
            col : entier représentant le combre de colonnes souhaitées
            p_bois : pourcentage de boisement souhaité
    %OUT    forest : la tableau bi-dimmensionnel représentant la forêt
"""
def gen_forest(row, col, p_bois):
    forest = []
    for row_number in range(row):
        forest.append([])
        for col_number in range(col):
            rand = random.random()
            if rand < p_bois:
                forest[row_number].append(1)
            else:
                forest[row_number].append(0)
    return forest