import unittest
from gen_forest.functions import gen_forest

class Test_Gen_Forest(unittest.TestCase):

    def test_gen_forest(self):
        forest_1 = [
            [1,1,1],
            [1,1,1],
            [1,1,1]
        ]
        self.assertEqual(forest_1, gen_forest(3,3,1))
        self.assertEqual(0, len(gen_forest(0,0,0)))