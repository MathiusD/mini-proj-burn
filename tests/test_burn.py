import unittest
from burn.functions import burn

class Test_Burn(unittest.TestCase):

    def test_burn(self):
        forest_1 = [
            [0,1,1],
            [0,1,2],
            [1,1,0]
        ]
        forest_2 = [
            [0,1,2],
            [0,2,3],
            [1,1,0]
        ]

        self.assertEqual(forest_2, burn(3,3, forest_1, 1))