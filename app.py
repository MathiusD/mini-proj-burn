from tkinter import *
from own.own_argparse.functions import Args
from burn.functions import burn
from gen_forest.functions import gen_forest
from data.config_pattern import pattern_args

arg = Args(pattern_args)
if arg.cell_size == None:
    if arg.cols >= arg.rows:
        lngt = 750 // arg.cols #dimension des carrés voulus
        W = 800
        H = (lngt*arg.rows)+10
    else:
        lngt = 710 // arg.rows
        W = (lngt*arg.cols)+50
        H = (lngt*arg.rows)+10
else:
    lngt = arg.cell_size
    W = (lngt*arg.cols)+50
    H = (lngt*arg.rows)+10

time_burn = 500

#Création et définition de la taille de la fenêtre
master = Tk()
master.geometry("%dx%d" % (W,H))
master.title("Burn")

#Création du Canvas
can = Canvas(master, width=(W-50), height=(H-10))
can.pack(side=LEFT)



def selection(event):
    global Forest
    global bool_pause
    if bool_pause == True:
        x = event.x
        y = event.y
        for ind_col in range(arg.cols):
            x1 = ind_col * lngt
            x2 = x1 + lngt
            for ind_row in range(arg.rows):
                y1 = ind_row * lngt
                y2 = y1 + lngt
                if Forest[ind_col][ind_row] == 1:
                    if x >= x1 and x < x2:
                        if y >= y1 and y < y2:
                            Forest[ind_col][ind_row] = 2
                            clear(can)
                            affichage(can, Forest)
                elif Forest[ind_col][ind_row] == 2:
                    if x >= x1 and x < x2:
                        if y >= y1 and y < y2:
                            Forest[ind_col][ind_row] = 1
                            clear(can)
                            affichage(can, Forest)   

#clear du canva
def clear(can):
    can.delete("all")

def affichage(can, forest):
    burning = False
    for ind_col in range(arg.cols):
        x = ind_col * lngt
        for ind_row in range(arg.rows):
            y = ind_row * lngt
            if forest[ind_col][ind_row] == 0:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="white")
            if forest[ind_col][ind_row] == 1:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="green")
            if forest[ind_col][ind_row] == 2:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="red")
                burning = True
            if forest[ind_col][ind_row] == 3:
                can.create_rectangle(x, y, x+lngt, y+lngt, fill="grey")
                burning = True
    return burning

def verif_pause():
    global bool_pause
    bool_pause = True

def launch():
    global bool_pause
    bool_pause = False
    master.after(0, runtime)

def law_update():
    global arg
    if arg.law == 1:
        arg.law = 2
        law_affiche()
    else:
        arg.law = 1
        law_affiche()

def law_affiche():
    global arg
    global bouton_law
    if arg.law == 1:
        bouton_law.config(text="Regle 1")
    else:
        bouton_law.config(text="Regle 2")

def runtime():
    global Forest
    global bool_pause
    global time_burn
    if bool_pause == False:
        burn(arg.cols, arg.rows, Forest, arg.law)
        clear(can)
        QuelqueChoseBrule = affichage(can, Forest)
        if QuelqueChoseBrule == False:
            bool_pause = True
        else:
            master.after(time_burn, runtime)
        

def gen():
    global Forest
    global bool_pause
    bool_pause = True
    Forest = gen_forest(arg.cols, arg.rows, arg.afforestation)
    clear(can)
    law_affiche()
    affichage(can, Forest)
    

f=Frame(master,width=50,height=H)
f.pack(side=LEFT)
bouton_lancement = Button(f, text="  Start  ", fg="black", command=launch)
bouton_lancement.pack(side = TOP)
bouton_pause = Button(f, text=" Pause ", fg="black", command=verif_pause)
bouton_pause.pack(side = TOP)
bouton_regen = Button(f, text="Restart", fg="black", command=gen)
bouton_regen.pack(side = TOP)
bouton_law = Button(f, fg="black", command=law_update)
bouton_law.pack(side = TOP)



can.bind('<Button-1>', selection)
can.bind('<B1-Motion>', selection)
gen()
master.mainloop()