# Mini projet "Simulation de Feux Forêt"

## Description

Ce projet a pour but de 'jouer' avec les automate cellulaire via une pratique pyromane. Heureusement la seule chose que nous brûlerons ici seront des octets innocents.

## Auteurs

* Paul Rosselle
* Mathieu Féry
