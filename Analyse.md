# Mini-Projet-Burn

## Analyse Préliminaire

* Module __own_argparse__ qui permet de "configurer" à proprement parler notre 'appel' et de récupérer les informations dont on a besoin :
*
  * Ce module sera une bibliothèque de fonction que nous exploiterons dans un fichier nommé __own_argparse.py__ au sein du module et possèdant la fonction app().
* Module __own_random__ qui permet de simuler un évenement avec une proba selon plusieurs 'loi' et le module __burn__ s'en servira sans remords.
* Module __burn__ qui permet de gérer notre pyromanie avancée.
*
  * Ce module dépendera de notre module __own_random__.
*
  * Ce module nous permettra de mettre à jour l'état de notre lieu à travers une fontion t_+_1().
* Module __gen_forest__ qui génère une forêt de manière aléatoire.
* Module __own_tk__ qui sera une bibliothèque de composants tkkinter que nous exploiterons dans notre application.
*
  * Ce dernier dépendera du module __own_argparse__, __gen_forest__ et __burn__.

## Organisation de l'équipe

* Paul souhaite s'occuper de l'interface graphique.
* Paul souhaite s'occuper de burn.
* Mathieu s'occupe de argparse.
* Mathieu s'occupe de gen_forest.
* Mathieu s'occupe de random.

## Diagramme de cas d'utilisation

* Bob le pyromane veut déclencher un feu de forêt et veut avoir une idée de la vitesse de propagation du feu.
* Les pompiers et les gardes forestier afin de contrecarer le plus vite possible un incendie.

## Diagramme d'activité

* Génération de la forêt.
* Selection des foyers.
* Lancement de la simulation.
* Pour chaque unité de temps on appelle burn qui nous donne l'état suivant de la forêt et ce tant que le feu n'est pas éteint.

## Diagrame de séquence

* Nos utilisateurs appellent la simulation en ligne de commande puis selectionnent graphiquement les foyer sur l'interface.
* L'appel en ligne de commande générera notre forêt et puis lancera notre IHM.
* Lors du lancement de notre incendie on appellera tant que le feu est présent burn qui mettra à jour l'interface graphique.

## Analyse Fonctionnelle

### Module Burn

* Burn(row[int], col[int], forest[forest[Tableau_à_deux_dimension_d'int], law[int]) -> forest[Tableau_à_deux_dimension_d'int]
### Module Gen_Forest

* Gen(row[int], col[int], p_bois[float]) -> forest[Tableau_à_deux_dimension_d'int]

### Module Own_Argparse

* Args(pattern_args[dict]) -> arg[object_du_module_argparse]

### Module Own_Random

* Rule_2(nb_burn[int]) -> burn[bool]

### Module Own_Tk

* Fichier d'applicatif app.py

## Tests

* Nos jeux de tests doivent comme à l'accoutumée contenir au moins 3 lots de données 'type':

  * 1 lot qui est attendu par la fonction et qui passe le test sans encombre.
  * 1 lot qui est volontairement incorrect et qui échouera au tests sans plus de détail.
  * 1 lot 'mixte' qui sera composé de données étrange qui passera ou non selon la fonction (A titre d'exemple tester si un url est un chemin d'accès valide).

* Ainsi qu'idéalement des tests d'intégrations pour nos modules sur le même principe.
