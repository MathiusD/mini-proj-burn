from own.own_random.functions import Rule_1, Rule_2

def burn(row, col, forest, law):
    for ind_row in range(0,row):
        for ind_col in range(0,col):
            if forest[ind_row][ind_col] == 1:
                compt_feu_adjacent = 0
                if ind_row > 0:
                    if forest[ind_row - 1][ind_col] == 2:
                        compt_feu_adjacent += 1
                if ind_row < (row-1):
                    if forest[ind_row + 1 ][ind_col] == 2:
                        compt_feu_adjacent += 1
                if ind_col > 0:
                    if forest[ind_row][ind_col - 1] == 2:
                        compt_feu_adjacent += 1
                if ind_col < (col-1):
                    if forest[ind_row][ind_col + 1] == 2:
                        compt_feu_adjacent += 1

                if law == 1 and Rule_1(compt_feu_adjacent) == True:
                    forest[ind_row][ind_col] = 4
                if law == 2 and Rule_2(compt_feu_adjacent) == True:
                    forest[ind_row][ind_col] = 4
    for ind_row in range(0,row):
        for ind_col in range(0,col):
            if forest[ind_row][ind_col] == 3:
                forest[ind_row][ind_col] = 0
            if forest[ind_row][ind_col] == 2:
                forest[ind_row][ind_col] = 3
            if forest[ind_row][ind_col] == 4:
                forest[ind_row][ind_col] = 2
    return forest